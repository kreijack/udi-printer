"""
udi-printer - a tool to print udi datamatrix for label
Copyright (C) 2023 Goffredo Baroncelli <kreijack@inwind.it>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""

import datetime, json

from PySide2.QtGui import QPixmap, QColor, QPainter
from PySide2.QtGui import QFontMetricsF
from PySide2.QtCore import QXmlStreamReader, QRectF, QByteArray, QPointF, QLineF, Qt
from PySide2.QtSvg import QSvgRenderer
from PySide2.QtPrintSupport import QPrintDialog, QPrinter
from PySide2.QtWidgets import QWidget

from ailist import AIList, UDIValidator, AIListDict
import increasesn
from version import version

import ppf.datamatrix


class UdiPagePreview(QWidget):
    def __init__(self, parent=None):
        QWidget.__init__(self, parent)
        self._currentLayout = None
        self._parameters = None
        self._doBox = False

    def setLayout(self, layout, parameters, doBox):
        self._currentLayout = layout
        self._parameters = parameters
        self._doBox = doBox

    def _prepareMessage(self, count=0, incrementPolicy=0, indexToIncrement=None):
        msg = ""
        messages = []
        appendSeparator = False
        AIDone = set()
        for key, values in self._currentLayout["fieldProp"].items():
            aiid = values["src"]

            fmt = AIListDict[aiid][2]

            """
            le = self._mapAIWidgets[aiid][2]
            if le.validator().validate(le.text(), 0) != QValidator.Acceptable:
                return None
            txt = le.text()
            """
            txt = self._parameters[aiid]
            if txt is None:
                return None

            x = values["x"]
            y = values["y"]
            mode = "txt"
            if aiid.startswith("lb"):
                mode = "txt"
            elif aiid.startswith("ln"):
                continue
            else:
                mode = values["mode"]

            if aiid.startswith("lb"):
                messages.append((txt, x, y))
            else:
                if count > 0 and incrementPolicy != 0 and indexToIncrement == aiid:
                    for i in range(count):
                        txt = increasesn.increaseSNText(txt, incrementPolicy)

                s = self._reshapeMessage(aiid, txt, mode)

                messages.append((s, x, y))

                if not aiid in AIDone:
                    if appendSeparator:
                        msg += "\x1d"
                        appendSeparator = False
                    msg += aiid + txt
                    if ".." in fmt:
                        # variable length -> add a separator
                        # if this is not the last string
                        appendSeparator = True
                    AIDone.add(aiid)

        return msg, messages

    def _reshapeMessage(self, aiid, txt, mode):
        """
            mode == ["(xx)txt", "txt", "[HIDE]",
                "yyyy-mm-dd", "dd-mmm-yyyy", "mmm-dd-yyyy"]
        """
        months = ["jan", "feb", "mar", "apr", "may", "jun",
                     "jul", "ago", "sep", "oct", "nov", "dec"]
        if mode == "txt":
            return txt
        elif mode == "[HIDE]":
            return ""
        elif mode == "yyyy/mm/dd" and len(txt) == 6:
            return "20%s/%s/%s"%(txt[0:2], txt[2:4], txt[4:6])
        elif mode == "yyyy" and len(txt) == 6:
            return "20%s"%(txt[0:2])
        elif mode == "dd/mmm/yyyy" and len(txt) == 6:
            try:
                m = months[int(txt[2:4])-1]
            except:
                return "ERROR"
            return "%s/%s/20%s"%(txt[4:6], m, txt[0:2])
        elif mode == "mmm/dd/yyyy" and len(txt) == 6:
            try:
                m = months[int(txt[2:4])-1]
            except:
                return "ERROR"
            return "%s/%s/20%s"%(m, txt[4:6], txt[0:2])
        else:   # (xx)txt
            return "(%s)%s"%(aiid, txt)

    def doPrint(self):
        printer = QPrinter()
        dialog = QPrintDialog(printer, self)
        dialog.setWindowTitle("Print Document")
        dialog.exec_()

        if dialog.result() != dialog.Accepted:
            return

        self._doPrint(printer)

    def _doPrint(self, printer):
        r = self._prepareMessage()
        if r is None:
            QMessageBox.critical(self, "UDI Printer: Print",
                "ERROR: UDI Information not correct.\n" +
                "Impossible to print")
            return
        msg, messages = r

        qrw = self._currentLayout["qrWidth"]
        pad = self._currentLayout["pad"]
        txth = self._currentLayout["txtHeight"]
        space = self._currentLayout["space"]
        trimLine = self._currentLayout["trimLine"] != 0
        qrx0 = self._currentLayout["qrx"]
        qry0 = self._currentLayout["qry"]
        totalWidth = self._currentLayout["totalWidth"]
        totalHeight = self._currentLayout["totalHeight"]

        painter = QPainter()
        painter.begin(printer)

        pageLayout = printer.pageLayout()
        pageLayout.setUnits(pageLayout.Millimeter)
        page = pageLayout.paintRect()
        pageRect = printer.pageLayout().paintRectPixels(printer.resolution())

        if False:
            # this to help to understand where we are writing
            pen = painter.pen()
            pen.setWidth(0)
            pen.setColor("green")
            painter.setPen(pen)
            painter.drawRect(QRectF(0, 0,
                                    pageRect.width(), pageRect.height()))
            print("pageRect(logical)=", pageRect)
            print("page(mm)=", page)

        x = 0.0
        y = 0.0

        try:
            c = 0
            while True:
                c += 1

                painter.resetTransform()

                painter.scale(1.0 * pageRect.width()/page.width(),
                    1.0 * pageRect.height()/page.height())
                painter.translate(QPointF(x, y))

                size = self._drawCanvas(painter,
                    qrw, pad, txth,
                    msg, messages, trimLine, space,
                    qrx0, qry0, totalWidth, totalHeight,
                    self._doBox)

                if self._currentLayout["multiPrint"] == 0:
                    break

                msg, messages = self._prepareMessage(c,
                    self._currentLayout["incrementPolicy"],
                    self._currentLayout["indexToIncrement"])

                if self._currentLayout["dxStep"] > 0:
                    dx = self._currentLayout["dxStep"]
                else:
                    dx = size.width()
                if self._currentLayout["dyStep"] > 0:
                    dy = self._currentLayout["dyStep"]
                else:
                    dy = size.height()

                if x + dx + size.width() < page.width():
                    x += dx
                elif y + dy + size.height() < page.height():
                    x = 0
                    y += dy
                else:
                    break

        finally:
            painter.end()

    def paintEvent(self, ev):
        w = self
        painter = QPainter()
        painter.begin(w)

        try:
            painter.fillRect(0, 0, w.width(), w.height(), QColor("lightyellow"))

            if self._parameters is None or self._currentLayout is None:
                pen = painter.pen()
                pen.setWidth(min(w.width() / 40, w.height() / 40))
                pen.setColor("red")
                painter.setPen(pen)

                painter.drawLine(0, 0, w.width(), w.height())
                painter.drawLine(w.width(), 0, 0, w.height())

                return

            r = self._prepareMessage()
            if r is None:
                pen = painter.pen()
                pen.setWidth(min(w.width() / 40, w.height() / 40))
                pen.setColor("gray")
                painter.setPen(pen)
                painter.drawLine(0, 0, w.width(), w.height())
                painter.drawLine(w.width(), 0, 0, w.height())
                return
            msg, messages = r

            qrw = self._currentLayout["qrWidth"]
            pad = self._currentLayout["pad"]
            txth = self._currentLayout["txtHeight"]
            trimLine = self._currentLayout["trimLine"] != 0
            space = self._currentLayout["space"]
            qrx0 = self._currentLayout["qrx"]
            qry0 = self._currentLayout["qry"]
            totalWidth = self._currentLayout["totalWidth"]
            totalHeight = self._currentLayout["totalHeight"]

            self._drawCanvas(painter,
                qrw, pad, txth,
                msg, messages, trimLine, space,
                qrx0, qry0, totalWidth, totalHeight,
                self._doBox, w.width(), w.height())

        finally:
            painter.end()

    def _drawCanvas(self, painter, qrw, pad, txth,
                    qrCodeMessage, messages, trimLine,
                    space, qrx0, qry0, totalWidth, totalHeight,
                    drawBox=False, width=None, height=None):

        lines = []

        for key, values in self._currentLayout["fieldProp"].items():
            aiid = values["src"]
            if not aiid.startswith("ln"):
                continue

            x = self._currentLayout["fieldProp"][key]["x"]
            y = self._currentLayout["fieldProp"][key]["y"]
            x2 = self._currentLayout["fieldProp"][key]["x2"]
            y2 = self._currentLayout["fieldProp"][key]["y2"]
            lines.append((x, y, x2, y2))

        qrh = qrw
        dmsvg = ppf.datamatrix.DataMatrix(qrCodeMessage, gs1_datamatrix=True).svg()
        render = QSvgRenderer(QXmlStreamReader(QByteArray(dmsvg.encode("utf-8"))))

        minx = qrx0
        miny = qry0
        maxx = qrx0 + qrw
        maxy = qry0 + qrw

        fontSize = 0.8 * txth # unit mm
        font = painter.font()
        font.setPixelSize(fontSize)
        font.setRawName("Arial")
        painter.setFont(font)
        fm = QFontMetricsF(font)

        for txt, x, y in messages:
            fr = fm.boundingRect(txt)
            ha = fm.horizontalAdvance(txt)
            fr.moveTo(x,y)
            fr.setWidth(ha)

            if fr.x() + fr.width() > maxx:
                maxx = fr.x() + fr.width()
            if fr.y() + fr.height() > maxy:
                maxy = fr.y() + fr.height()
            if fr.x() < minx:
                minx = fr.x()
            if fr.y() < miny:
                miny = fr.y()

        for line in lines:
            for x, y in ((line[0], line[1]), (line[2], line[3])):
                if x < minx:
                    minx = x
                if x > maxx:
                    maxx = x
                if y < miny:
                    miny = y
                if y > maxy:
                    maxy = y

        offx = minx - space
        offy = miny - space
        if totalHeight > 0:
            maxheight = totalHeight + 2 * space
        else:
            maxheight = maxy - miny + 2 * space
        if totalWidth > 0:
            maxwidth = totalWidth + 2 * space
        else:
            maxwidth = maxx - minx + 2 * space


        if width and height:
            # this only if we need to consume all the width x height space
            ratio = 1.0 * width / (maxwidth + 2*pad)
            if ratio > 1.0 * height / (maxheight + 2*pad):
                ratio = 1.0 * height / (maxheight + 2*pad)
            painter.scale(ratio, ratio)

        painter.translate(QPointF(+pad-offx ,+pad-offy ))

        fm = QFontMetricsF(painter.font())
        pen = painter.pen()

        pen.setColor("black")
        painter.setPen(pen)

        render.render(painter, QRectF(qrx0, qry0, qrw, qrw))

        for txt, x, y in messages:
            painter.drawText(x, y + txth*0.8, txt)

        if drawBox:
            pen.setWidth(0)
            pen.setColor("blue")
            painter.setPen(pen)
            r = QRectF(offx+ space, offy + space,
                       maxwidth - 2*space, maxheight - 2*space)
            painter.drawRect(r)

        if trimLine:
            pen.setWidth(0)
            pen.setColor("red")
            painter.setPen(pen)

            painter.drawRect(QRectF(offx, offy, maxwidth, maxheight))

            def writeTrimLine(x0, y0, dx, dy, perc=1.0):
                pad2 = pad * perc

                painter.drawLine(QPointF(x0, offy),
                                 QPointF(x0, offy-pad2))
                painter.drawLine(QPointF(x0 + dx, offy),
                                 QPointF(x0+dx, offy-pad2))
                painter.drawLine(QPointF(x0, offy+maxheight),
                                 QPointF(x0, offy+maxheight+pad2))
                painter.drawLine(QPointF(x0 + dx, offy+maxheight),
                                 QPointF(x0+dx, offy+maxheight+pad2))

                painter.drawLine(QPointF(offx, y0), QPointF(offx-pad2, y0))
                painter.drawLine(QPointF(offx, y0+dy), QPointF(offx-pad2, y0+dy))
                painter.drawLine(QPointF(offx+maxwidth, y0),
                                 QPointF(offx+maxwidth+pad2, y0))
                painter.drawLine(QPointF(offx+maxwidth, y0+dy),
                                 QPointF(offx+maxwidth+pad2, y0+dy))

            writeTrimLine(offx+space, offy+space,
                          maxwidth-2*space, maxheight-2*space)

            # qr code
            writeTrimLine(qrx0, qry0, qrw, qrw, .66)

            for txt, x, y in messages:
                fr = fm.boundingRect(txt)
                fr.moveTo(x,y)
                writeTrimLine(fr.x(), fr.y(),
                              fr.width(), fr.height(), .33)

            #pen.setStyle(Qt.DashDotDotLine)
            pen.setWidth(0)
            pen.setColor("black")
            painter.setPen(pen)
            for (x, y, x2, y2) in lines:
                painter.drawLine(x, y, x2, y2)

        return QRectF(minx-pad, miny-pad, maxwidth + 2 * pad, maxheight + 2 * pad)

def main(args):
    from PySide2.QtWidgets import QApplication
    from PySide2.QtGui import QPageLayout, QPageSize
    from PySide2.QtCore import QMarginsF
    import sys
    import re

    if len(args) < 3:
        print("Usage: %s <fileout.pdf> <(01)param>"%(args[0]))
        return

    app = QApplication()
    w = UdiPagePreview()
    layout = {
            "qrWidth": 30,
            "txtHeight": 15,
            "space": 3,
            "pad": 5,
            "trimPerc": 50,
            "layout": 5,
            "trimLine": 1,
            "multiPrint": 1,
            "dxStep": 1000000,
            "dyStep": 1000000,
            "indexToIncrement": "17",
            "incrementPolicy": 0,
            "qrx": 0,
            "qry": 0,
            "totalWidth": 0,
            "totalHeight": 0,
            "values": {
                1: "",
            },
            "fieldProp": {
                    1: {
                        "caption": "",
                        "x": 0,
                        "y": 40,
                        "mode": "(xx)txt",
                        "src": "21",
                    }
            }
        }
    parameters = { "21": args[2][:20] }

    m = re.split(r"([(][0-9]+[)][^(]*)", args[2])
    if len(m) > 1:
        i = 0
        while i < len(m):
            if len(m[i]) != 0:
                print("Error in parameter '%s'"%(args[2]))
                return
            i += 2
        i = 1
        parameters = {}
        layout["values"] = {}
        layout["fieldProp"] = {}
        while i < len(m):
            j = m[i].find(")")
            idx = m[i][1:j]
            arg = m[i][j+1:]

            parameters[idx] = arg
            layout["fieldProp"][i] = {
                        "caption": "",
                        "x": 0,
                        "y": 40 + 20 * i/2 ,
                        "mode": "(xx)txt",
                        "src": idx,
            }
            layout["values"][i] = ""

            i += 2

    w.setLayout(layout, parameters, False )
    printer = QPrinter()
    printer.setPageLayout(QPageLayout(QPageSize(QPageSize.A4), QPageLayout.Portrait,
                          QMarginsF(0.14, 0.14, 0.14, 0.14)))
    printer.setOutputFileName(args[1])

    w._doPrint(printer)
    #app.exec_()

if __name__ == "__main__":
    import sys
    main(sys.argv)
