"""
udi-printer - a tool to print udi datamatrix for label
Copyright (C) 2023 Goffredo Baroncelli <kreijack@inwind.it>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""

def increaseSNText(message, incrementPolicy):
    if incrementPolicy == 0:
        return message

    matcher = {
        # Plain integer (0..9,10..)
        1 : (10, lambda x: x.isdigit()),
        # Hexadecimal (0..9,a..f,10..)
        2 : (16, lambda x: x.isdigit() or (x >= 'a' and x <= 'f')),
        # HEXADECIMAL (0..9,A..F,10..)
        3 : (16, lambda x: x.isdigit() or (x >= 'A' and x <= 'F')),
        # Alphanumeric (0..9,a..z,10..)
        4 : (36, lambda x: x.isdigit() or (x >= 'a' and x <= 'z')),
        # ALPHANUMERIC (0..9,A..Z,10..)
        5 : (36, lambda x: x.isdigit() or (x >= 'A' and x <= 'Z')),
    }[incrementPolicy]

    i = len(message) - 1
    while i >= 0 and matcher[1](message[i]):
        i -= 1

    if i == len(message) - 1:
        # nothing that seems a SN
        return message

    j = i+1
    n = 0
    while j < len(message):
        if message[j].isdigit():
            n = n * matcher[0] + int(message[j])
        else:
            n = n * matcher[0] + ord(message[j].upper()) - ord('A') + 10
        j += 1

    n = n + 1
    m1 = ""
    while n > 0:
        n1 = n % matcher[0]
        if n1 < 10:
           m1 = chr(ord('0') + n1) + m1
        else:
           m1 = chr(ord('A') + n1 - 10) + m1
        n //= matcher[0]

    if incrementPolicy in (2, 4):
        m1 = m1.lower()

    message = message[:i+1] + ('0' * len(message) + m1)[-len(message) + i+1:]

    return message


def test_increaseSN_do_nothing():
    assert(increaseSNText("123456789", 0) == "123456789")

def test_increaseSN_mode_dec_only():

    assert(increaseSNText("123456789", 1) == "123456790")

    assert(increaseSNText("999999999", 1) == "000000000")

    assert(increaseSNText("123abcd70", 1) == "123abcd71")

    assert(increaseSNText("123abcd99", 1) == "123abcd00")

    assert(increaseSNText("123abcd9-", 1) == "123abcd9-")

def test_increaseSN_mode_hex_lower_only():

    assert(increaseSNText("123456789", 2) == "12345678a")

    assert(increaseSNText("999999999", 2) == "99999999a")

    assert(increaseSNText("9999999ff", 2) == "999999a00")

    assert(increaseSNText("123abcz70", 2) == "123abcz71")

    assert(increaseSNText("123abczff", 2) == "123abcz00")

    assert(increaseSNText("123abcAff", 2) == "123abcA00")

def test_increaseSN_mode_hex_upper_only():

    assert(increaseSNText("123456789", 3) == "12345678A")

    assert(increaseSNText("999999999", 3) == "99999999A")

    assert(increaseSNText("9999999FF", 3) == "999999A00")

    assert(increaseSNText("123abcz70", 3) == "123abcz71")

    assert(increaseSNText("123abczFF", 3) == "123abcz00")

    assert(increaseSNText("123abcaFF", 3) == "123abca00")

def test_increaseSN_mode_alnum_upper_only():

    assert(increaseSNText("123456789", 5) == "12345678A")

    assert(increaseSNText("999999999", 5) == "99999999A")

    assert(increaseSNText("9999999FF", 5) == "9999999FG")

    assert(increaseSNText("123abcz70", 5) == "123abcz71")

    assert(increaseSNText("123abczZZ", 5) == "123abcz00")

    assert(increaseSNText("123abcaZZ", 5) == "123abca00")

    assert(increaseSNText("ZZZZZZZZ-", 5) == "ZZZZZZZZ-")

    assert(increaseSNText("ZZZZZZZZZ", 5) == "000000000")


def test_increaseSN_mode_alnum_lower_only():

    assert(increaseSNText("123456789", 4) == "12345678a")

    assert(increaseSNText("999999999", 4) == "99999999a")

    assert(increaseSNText("9999999ff", 4) == "9999999fg")

    assert(increaseSNText("123abcZ70", 4) == "123abcZ71")

    assert(increaseSNText("123abcZzz", 4) == "123abcZ00")

    assert(increaseSNText("123abcAzz", 4) == "123abcA00")

    assert(increaseSNText("zzzzzzzz-", 4) == "zzzzzzzz-")

    assert(increaseSNText("zzzzzzzzz", 4) == "000000000")


def test_increaseSN():
    print("test_increaseSN_do_nothing")
    test_increaseSN_do_nothing()
    print("test_increaseSN_mode_dec_only")
    test_increaseSN_mode_dec_only()
    print("test_increaseSN_mode_hex_lower_only")
    test_increaseSN_mode_hex_lower_only()
    print("test_increaseSN_mode_hex_upper_only")
    test_increaseSN_mode_hex_upper_only()
    print("test_increaseSN_mode_alnum_upper_only")
    test_increaseSN_mode_alnum_upper_only()
    print("test_increaseSN_mode_alnum_lower_only")
    test_increaseSN_mode_alnum_lower_only()

if __name__ == "__main__":
    test_increaseSN()
