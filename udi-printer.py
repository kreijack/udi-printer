"""
udi-printer - a tool to print udi datamatrix for label
Copyright (C) 2023 Goffredo Baroncelli <kreijack@inwind.it>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""

import datetime, json

from PySide2.QtWidgets import QApplication
from PySide2.QtWidgets import QMainWindow, QWidget, QMessageBox
from PySide2.QtWidgets import QGridLayout, QCheckBox
from PySide2.QtWidgets import QPushButton, QLabel, QLineEdit, QListWidget
from PySide2.QtWidgets import QComboBox, QGroupBox
from PySide2.QtCore import Qt
from PySide2.QtGui import QIntValidator, QValidator

from ailist import AIList, UDIValidator, AIListDict
from layouts import loadLayouts, LayoutDialog, Caller
import increasesn
from version import version
import udipagepreview

class MainWindow(QMainWindow):
    def __init__(self, parent=None):
        #self._AIAllowed = ("01", "10", "11", "17")
        QMainWindow.__init__(self, parent)
        self._dlgLayout = None
        self._initGui()
        self._layouts = loadLayouts()
        for k in self._layouts.keys():
            self._layoutId.addItem(k)
        self._changeLayout()

    def lineEditCheckState(self, le):
        st = le.validator().validate(le.text(), 0)
        if st == QValidator.Acceptable:
            color = '#c4df9b' # green
        elif st == QValidator.Intermediate:
            color = '#fff79a' # yellow
        else:
            color = '#f6989d' # red
        le.setStyleSheet('QLineEdit { background-color: %s }' % color)

    def _initGui(self):
        w = QWidget()
        l = QGridLayout()
        w.setLayout(l)
        self.setCentralWidget(w)

        self._upp = udipagepreview.UdiPagePreview()

        l.addWidget(QLabel("Layout"), 1, 1);
        self._layoutId = QComboBox()
        self._layoutId.setCurrentIndex(0)
        l.addWidget(self._layoutId, 1, 2, 1, 9)
        self._layoutId.currentIndexChanged.connect(lambda *x: self._changeLayout())

        ###

        self._boxCB = QCheckBox("Draw box around items")
        self._boxCB.setChecked(False)
        l.addWidget(self._boxCB, 2, 1)
        self._boxCB.stateChanged.connect(lambda *x : self._uppRepaint())

        ###
        gb = QGroupBox("UDI Information")
        l.addWidget(gb, 3, 1, 1, 10)
        l2 = QGridLayout()
        gb.setLayout(l2)

        self._mapAIWidgets = dict()
        row = 10
        for aiid, descr, fmt, sdescr in AIList:
            w1 = QLabel(descr)
            l2.addWidget(w1, row, 10)
            w2 = QLabel("("+aiid+")")
            l2.addWidget(w2, row, 11, 1, 1, Qt.AlignRight)
            le = QLineEdit()
            le.setValidator(UDIValidator(fmt))
            le.textChanged.connect(
                Caller(lambda y, _: self.lineEditCheckState(y), le))
            self._mapAIWidgets[aiid] = (w1, w2, le)
            l2.addWidget(le, row, 13)
            le.textEdited.connect(lambda *x : self._uppRepaint())

            row += 1


        ####

        gb = QGroupBox("Preview")
        l.addWidget(gb, 5, 1, 1, 10)
        l2 = QGridLayout()
        gb.setLayout(l2)
        l2.addWidget(self._upp, 5, 1, 1, 10)
        l.setRowStretch(5, 10)

        b = QPushButton("Print ...")
        b.clicked.connect(self._print)
        l.addWidget(b, 10, 1)

        #b = QPushButton("Preview ...")
        #l.addWidget(b, 10, 2)

        b = QPushButton("Edit layouts ...")
        b.clicked.connect(self._editLayouts)
        l.addWidget(b, 10, 3)

        b = QPushButton("Close")
        b.clicked.connect(self.close)
        l.addWidget(b, 10, 10)

        self.setWindowTitle("UDI Printer - v"+version)

    def _changeLayout(self, resetField=True):
        i = self._layoutId.currentText()
        if not i in self._layouts:
            return
        currentLayout = self._layouts[i]

        for aiid, descr, fmt, sdescr in AIList:
            if not aiid in currentLayout["values"].keys() or aiid.startswith("ln"):
                for i in self._mapAIWidgets[aiid]:
                    i.hide()
                self._mapAIWidgets[aiid][2].setText("")
                continue

            for i in self._mapAIWidgets[aiid]:
                i.show()
            if resetField:
                self._mapAIWidgets[aiid][2].setText(
                    currentLayout["values"][aiid])
            if aiid.startswith("lb"):
                for k, v in currentLayout["fieldProp"].items():
                    if v["src"] == aiid:
                        self._mapAIWidgets[aiid][0].setText(v["caption"])
                        break

        self._uppRepaint()

    def _uppRepaint(self):
        self._uppSetLayout()
        self._upp.repaint()

    def _uppSetLayout(self):
        i = self._layoutId.currentText()
        if not i in self._layouts:
            return
        currentLayout = self._layouts[i]

        parameters = dict()
        for key, values in currentLayout["fieldProp"].items():
            aiid = values["src"]
            le = self._mapAIWidgets[aiid][2]
            parameters[aiid] = le.text()

            st = le.validator().validate(le.text(), 0)
            if st != QValidator.Acceptable:
                # even if a parameter is not valid, prevent drawing anything
                currentLayout = None
                parameters = None
                break

        self._upp.setLayout(currentLayout, parameters, self._boxCB.isChecked())


    def _editLayouts(self):
        if not self._dlgLayout:
            self._dlgLayout = LayoutDialog(self)
            self._dlgLayout.layoutUpdated.connect(self.reloadLayout)

        self._dlgLayout.show()

    def reloadLayout(self, s):
        # reload layouts
        idx = self._layoutId.currentText()
        self._layouts = json.loads(s)

        self._layoutId.blockSignals(True)
        self._layoutId.clear()
        i = 0
        newIndex = 0

        for k in self._layouts.keys():
            self._layoutId.addItem(k)
            if k == idx:
                newIndex = i
            i += 1
        self._layoutId.setCurrentIndex(newIndex)
        self._layoutId.blockSignals(False)

        self._changeLayout(resetField=False)

    def _print(self):
        self._uppSetLayout()
        self._upp.repaint()
        self._upp.doPrint()

def main():
    app = QApplication()
    w = MainWindow()
    w.resize(400, 600)
    w.show()
    app.exec_()

main()
