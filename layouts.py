"""
udi-printer - a tool to print udi datamatrix for label
Copyright (C) 2023 Goffredo Baroncelli <kreijack@inwind.it>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""

import json

from PySide2.QtWidgets import QDialog, QScrollArea, QWidget, QMessageBox
from PySide2.QtWidgets import QGridLayout, QCheckBox, QListWidget, QHBoxLayout
from PySide2.QtWidgets import QPushButton, QLabel, QLineEdit, QListWidget
from PySide2.QtWidgets import QComboBox, QGroupBox, QMainWindow
from PySide2.QtWidgets import QSpinBox, QSplitter, QDoubleSpinBox
from PySide2.QtGui import QIntValidator, QRegExpValidator, QValidator
from PySide2.QtCore import Qt, QItemSelectionModel, Signal

from ailist import AIList, UDIValidator, AIListDict
from version import version

_default = {
    "default": {
        "qrWidth": 30,
        "txtHeight": 15,
        "space": 3,
        "pad": 5,
        "trimPerc": 50,
        "layout": 5,
        "trimLine": 1,
        "multiPrint": 1,
        "dxStep": 0,
        "dyStep": 0,
        "indexToIncrement": "17",
        "incrementPolicy": 0,
        "qrx": 0,
        "qry": 0,
        "totalWidth": 0,
        "totalHeight": 0,
        "values": {
            1: "",
        },
        "fieldProp": {
                1: {
                    "caption": "",
                    "x": 0,
                    "y": 0,
                    "mode": "(xx)txt",
                    "src": "01",
                }
        }
    }
}


class Caller:
    def __init__(self, effe, *args1, **kwargs1):
        self._effe = effe
        self._args1 = args1
        self._kwargs1 = kwargs1
    def __call__(self, *args2, **kwargs2):
        return self._effe(*self._args1, *args2, **self._kwargs1, **kwargs2)

def loadLayouts():
    global _default
    try:
        r2 = json.loads(open("layouts.json").read())
    except:
        return _default

    return r2;

def updateComboBox(cb, l, default=None):
    cb.clear()
    for i in l:
        cb.addItem(i)

    cb.setCurrentIndex(0)
    if default:
        i = l.index(default)
        if i >= 0:
            cb.setCurrentIndex(i)

class LayoutDialog(QMainWindow):

    layoutUpdated = Signal(str)

    def __init__(self, parent=None):
        self._title = "UDI Printer - v" + version + " : layout editor"
        self._updated = False
        self._mapFPRow = dict()
        self._skipMarkDirty = True

        QMainWindow.__init__(self, parent=parent)
        self._fieldIndex = None
        self._layouts = loadLayouts()
        self._initGui()

        updateComboBox(self._layoutId, list(self._layouts.keys()))

        self._loadDataFromCurrentLayout()
        self._updateUDIInformation()
        self._skipMarkDirty = False
        self._markUpdated()

    def _markDirty(self):
        if self._skipMarkDirty:
            return

        self._updated = False
        self.setWindowTitle(self._title+" *")
        self._saveDataToLayoutName(self._layoutId.currentText())
        self.layoutUpdated.emit(json.dumps(self._layouts))

    def _markUpdated(self):
        self._updated = True
        self.setWindowTitle(self._title)
        self.layoutUpdated.emit(json.dumps(self._layouts))

    def _initGui(self):
        w = QWidget()
        self.setCentralWidget(w)
        l2 = QGridLayout()
        w.setLayout(l2)

        l2.addWidget(QLabel("Layout"), 9, 10);
        self._layoutId = QComboBox()
        l2.addWidget(self._layoutId, 9, 11, 1, 6)
        self._layoutId.currentIndexChanged.connect(self._changeLayout)

        ###
        
        gb = QGroupBox("Layout parameter")
        l2.addWidget(gb, 12, 10, 1, 7)
        l4 = QGridLayout()
        gb.setLayout(l4)

        def mkQSp(lbl, min_, max_, def_, l, r, c, c2=-1):
            w = QDoubleSpinBox()
            w.setMaximum(max_)
            w.setMinimum(min_)
            w.setValue(def_)

            if c2 == -1:
                c2 = c+1
            l.addWidget(QLabel(lbl), r, c);
            l.addWidget(w, r, c2);
            return w;

        self._qrWidthSp = mkQSp("QR Code width", 1, 999, 20, l4, 10, 10, 11)
        self._txtHeightSp = mkQSp("Text height", 1, 999, 15, l4, 10, 12, 13)
        self._qrx = mkQSp("QR X Pos", -99999, 99999, 0, l4, 12, 10)
        self._qry = mkQSp("QR Y Pos", -99999, 99999, 0, l4, 12, 12)
        self._spaceSp = mkQSp("Gap QR and Text", 0, 999, 3, l4, 14, 10, 11)
        self._padSp = mkQSp("Pad", 0, 999, 2, l4, 14, 12, 13)
        self._totalWidth = mkQSp("Width", 0, 99999, 0, l4, 18, 10)
        self._totalHeight = mkQSp("Height", 0, 99999, 0, l4, 18, 12)
        self._trimLineCB = QCheckBox("Trim line")
        self._trimLineCB.setChecked(False)
        l4.addWidget(self._trimLineCB, 20, 10)

        ####

        self._multiPrintCB = QGroupBox("Print multiple UDI")
        self._multiPrintCB.setCheckable(True)
        self._multiPrintCB.setChecked(True)
        l2.addWidget(self._multiPrintCB, 22, 10, 1, 7)
        l4 = QGridLayout()
        self._multiPrintCB.setLayout(l4)

        self._dxStepSp = mkQSp("DX Step", 0, 999, 0, l4, 23, 10, 11)
        self._dyStepSp = mkQSp("DY Step", 0, 999, 0, l4, 23, 12, 13)

        l4.addWidget(QLabel("Index to increment"), 25, 10);
        self._indexToIncrement = QComboBox()
        l4.addWidget(self._indexToIncrement, 25, 11, 1, 3)

        l4.addWidget(QLabel("Increment policy"), 26, 10);
        self._incrementPolicy = QComboBox()
        l4.addWidget(self._incrementPolicy, 26, 11, 1, 3)
        self._incrementPolicy.addItems(["No increment",
                                        "Plain integer (0..9,10..)",
                                        "Hexadecimal (0..9,a..f,10..)",
                                        "HEXADECIMAL (0..9,A..F,10..)",
                                        "Alphanumeric (0..9,a..z,10..)",
                                        "ALPHANUMERIC (0..9,A..Z,10..)"])

        ###


        splitter = QSplitter(Qt.Orientation.Vertical)
        l2.addWidget(splitter, 30, 10, 1, 7)

        self._AIListView = QListWidget()
        for aiid, descr, fmt, sdescr in AIList:
            if sdescr != "":
                sdescr = " [%s]"%(sdescr)
            self._AIListView.addItem("%6s) %s%s"%(aiid, descr, sdescr))

        l4.addWidget(self._AIListView, 10, 10, 1, 4)
        self._AIListView.doubleClicked.connect(self._addFieldProp)

        splitter.addWidget(self._AIListView)

        gb = QGroupBox("UDI Default Information")
        splitter.addWidget(gb)
        l = QHBoxLayout()
        gb.setLayout(l)
        sa = QScrollArea()
        l.addWidget(sa)
        w = QWidget()
        sa.setWidgetResizable(True)
        sa.setWidget(w)
        self._fieldPropGrid = QGridLayout()
        w.setLayout(self._fieldPropGrid)

        l3 = QHBoxLayout()
        l2.addLayout(l3, 99, 10, 1, 7)
        btn = QPushButton("Save layout as ...")
        btn.clicked.connect(self._saveAs)
        l3.addWidget(btn)

        self._saveAsLe = QLineEdit()
        self._saveAsLe.setValidator(
            QRegExpValidator("[A-Za-z0-9_\--]+"))
        l3.addWidget(self._saveAsLe)

        btn = QPushButton("Delete layout ...")
        btn.clicked.connect(self._deleteLayout)
        l3.addWidget(btn)

        l3.addStretch(100)
        btn = QPushButton("Close")
        btn.clicked.connect(self._close)
        l3.addWidget(btn)

        for x in self.findChildren(QLineEdit):
            if x == self._saveAsLe:
                continue
            x.textEdited.connect(lambda *x : self._markDirty())
        for x in self.findChildren(QSpinBox):
            x.valueChanged.connect(lambda *x : self._markDirty())
        for x in self.findChildren(QDoubleSpinBox):
            x.valueChanged.connect(lambda *x : self._markDirty())
        for x in self.findChildren(QComboBox):
            if x == self._layoutId:
                continue
            x.currentIndexChanged.connect(lambda *x : self._markDirty())
        for x in self.findChildren(QCheckBox):
            x.stateChanged.connect(lambda *x : self._markDirty())
        self._multiPrintCB.toggled.connect(lambda *x : self._markDirty())
        for x in self.findChildren(QListWidget):
            x.currentItemChanged.connect(lambda *x : self._markDirty())

    def _changeLayout(self):
        if self._skipMarkDirty:
            return
        self._skipMarkDirty = True
        self._loadDataFromCurrentLayout()
        self._skipMarkDirty = False

    def _findNextFieldPropId(self):
        i = self._layoutId.currentText()
        assert(i in self._layouts)
        layout = self._layouts[i]

        keys = list(map(int, layout["fieldProp"].keys()))
        if len(keys) > 0:
            return "%d"%(max(keys) +1)
        else:
            return "100"

    def _addFieldProp(self):
        aiid = None
        for i in self._AIListView.selectedItems():
            aiid = i.text().split(")")[0].strip()
            break
        if not aiid:
            return

        layout = self._getCurrentLayout()

        caption = AIListDict[aiid][1]

        fieldPropId = self._findNextFieldPropId()
        assert(not fieldPropId in layout["fieldProp"])

        if not aiid in layout["values"]:
            layout["values"][aiid] = ""
        layout["fieldProp"][fieldPropId] = {
                "caption": caption,
                "x": 0,
                "y": 0,
                "src": aiid
            }
        if aiid.startswith("lb"):
            pass
        elif aiid.startswith("ln"):
            layout["fieldProp"][fieldPropId]["x2"] = 0
            layout["fieldProp"][fieldPropId]["y2"] = 0
        else:
            layout["fieldProp"][fieldPropId]["mode"] = "(xx)txt"

        self._skipMarkDirty = True
        self._populateFieldPropGrid()
        self._skipMarkDirty = False
        self._markDirty()

    def _clearGrid(self, grid):
        for r in range(grid.rowCount()):
            for c in range(grid.columnCount()):
                i = grid.itemAtPosition(r, c)
                if i:
                    w = i.widget()
                    grid.removeItem(i)
                    w.hide()
                    del w

    def _getCurrentLayout(self):
        i = self._layoutId.currentText()
        assert(i in self._layouts)
        return self._layouts[i]

    def _populateFieldPropGrid(self):
        layout = self._getCurrentLayout()

        self._clearGrid(self._fieldPropGrid)

        row = 10
        self._mapFPRow = dict()
        aiidDone = set()
        l = list(layout["fieldProp"].keys())
        l.sort(key=lambda k: layout["fieldProp"][k]["src"])
        for k in l:
            v = layout["fieldProp"][k]

            aiid = v["src"]
            descr = AIListDict[aiid][1]
            fmt = AIListDict[aiid][2]

            pb = QPushButton("-")
            pb.clicked.connect(Caller(lambda x, y: x._removeFieldPropRow(y), self, row))
            self._fieldPropGrid.addWidget(pb, row, 1)
            if aiid.startswith("lb"):
                caption = QLineEdit()
                caption.setText(descr)
                if aiid in aiidDone:
                    caption.setEnabled(False)
                else:
                    caption.textEdited.connect(lambda *x : self._markDirty())
                    caption.textEdited.connect(
                        Caller(lambda *x, **kwargs :
                                   self._updateAIILValues(kwargs["whichone"], kwargs["idx"]),
                               whichone=caption, idx=2))
            else:
                caption = QLabel(descr)
            self._fieldPropGrid.addWidget(caption, row, 10)
            w2 = QLabel("("+aiid+")")
            self._fieldPropGrid.addWidget(w2, row, 11, 1, 1, Qt.AlignRight)

            le = QLineEdit()
            le.setValidator(UDIValidator(fmt))
            le.textChanged.connect(
                Caller(lambda y, _: self.lineEditCheckState(y), le))
            le.setText(layout["values"][aiid])
            if aiid in aiidDone:
                le.setEnabled(False)
            else:
                le.textEdited.connect(
                    Caller(lambda *x, **kwargs : self._updateAIILValues(kwargs["whichone"], kwargs["idx"]),
                        whichone=le, idx=4))
            if aiid.startswith("ln"):
                le.setEnabled(False)

            self._fieldPropGrid.addWidget(le, row, 15)

            self._fieldPropGrid.addWidget(caption, row, 14)
            x = QDoubleSpinBox()
            x.setMaximum(99999)
            x.setMinimum(-99999)
            x.setValue(v["x"])
            y = QDoubleSpinBox()
            y.setMaximum(99999)
            y.setMinimum(-99999)
            y.setValue(v["y"])

            self._fieldPropGrid.addWidget(x, row, 20)
            self._fieldPropGrid.addWidget(y, row, 21)

            if aiid.startswith("lb"):
                self._mapFPRow[row] = [k, aiid, caption, w2, le, x, y]
            elif aiid.startswith("ln"):
                x2 = QDoubleSpinBox()
                x2.setMaximum(99999)
                x2.setMinimum(-99999)
                x2.setValue(v["x2"])
                y2 = QDoubleSpinBox()
                y2.setMaximum(99999)
                y2.setMinimum(-99999)
                y2.setValue(v["y2"])

                self._fieldPropGrid.addWidget(x2, row, 23)
                self._fieldPropGrid.addWidget(y2, row, 24)
                self._mapFPRow[row] = [k, aiid, caption, w2, le, x, y, x2, y2]
            else:
                mode = QComboBox()
                self._fieldPropGrid.addWidget(mode, row, 23, 1, 2)
                l = ["(xx)txt", "txt", "[HIDE]", ]
                if "(YYMMDD)" in descr:
                    l += ["yyyy/mm/dd", "dd/mmm/yyyy", "mmm/dd/yyyy",
                          "yyyy"]
                mode.addItems(l)

                i = l.index(v["mode"])
                if i >= 0:
                    mode.setCurrentIndex(i)

                self._mapFPRow[row] = [k, aiid, caption, w2, le, x, y, mode]

            row += 1
            aiidDone.add(aiid)

        w = self._fieldPropGrid.parent()
        for x in w.findChildren(QSpinBox):
            x.valueChanged.connect(lambda *x : self._markDirty())
        for x in w.findChildren(QDoubleSpinBox):
            x.valueChanged.connect(lambda *x : self._markDirty())
        for x in w.findChildren(QComboBox):
            x.currentIndexChanged.connect(lambda *x : self._markDirty())
        for x in w.findChildren(QCheckBox):
            x.stateChanged.connect(lambda *x : self._markDirty())
        for x in w.findChildren(QListWidget):
            x.currentItemChanged.connect(lambda *x : self._markDirty())

    def _updateAIILValues(self, whichone, idx):
        if self._skipMarkDirty:
            return
        self._skipMarkDirty = True
        for k, v in self._mapFPRow.items():
            if v[idx] == whichone:
                theValue = whichone.text()
                theKey = v[1]
                break
        for k, v in self._mapFPRow.items():
            if v[idx] != whichone and v[1] == theKey:
                v[idx].setText(theValue)
        self._skipMarkDirty = False

    def _removeFieldPropRow(self, row):
        if not row in self._mapFPRow:
            return
        k = self._mapFPRow[row][0]
        del self._getCurrentLayout()["fieldProp"][k]
        self._skipMarkDirty = True
        self._loadDataFromCurrentLayout()
        self._skipMarkDirty = False
        self._markDirty()

    def _deleteLayout(self):
        if not self._layoutId.currentText() in self._layouts:
            return

        if len(self._layouts) < 2:
            QMessageBox.critical(self, "UDI Printer: delete layout",
                "ERROR: you cannot delete the last layout ('%s')"%(
                    self._layoutId.currentText()))
            return

        if not self._updated:
            r = QMessageBox.question(self, "UDI Printer: delete layout",
                "There are data unsaved; are you sure to delete layout '%s'"%(
                    self._layoutId.currentText()))
        else:
            r = QMessageBox.question(self, "UDI Printer: delete layout",
                "Are you sure to delete layout '%s'"%(
                    self._layoutId.currentText()))

        if r != QMessageBox.Yes:
            return

        del self._layouts[self._layoutId.currentText()]

        open("layouts.json", "w").write(json.dumps(
                self._layouts, sort_keys=True, indent='    '))

        updateComboBox(self._layoutId, list(self._layouts.keys()))

        self._skipMarkDirty = True
        self._loadDataFromCurrentLayout()
        self._skipMarkDirty = False
        self._markUpdate()

    def lineEditCheckState(self, le):
        st = le.validator().validate(le.text(), 0)
        if st == QValidator.Acceptable:
            color = '#c4df9b' # green
        elif st == QValidator.Intermediate:
            color = '#fff79a' # yellow
        else:
            color = '#f6989d' # red
        le.setStyleSheet('QLineEdit { background-color: %s }' % color)

    def _close(self):
        if not self._updated:
            r = QMessageBox.question(self, "UDI Printer: close",
                "There are data unsaved; are you sure to close ?")
            if r != QMessageBox.Yes:
                return
        self.close()

    def _loadDataFromCurrentLayout(self):
        prevSkipMarkDirty = self._skipMarkDirty
        self._skipMarkDirty = True

        l = self._getCurrentLayout()

        self._saveAsLe.setText(self._layoutId.currentText())
        self._qrWidthSp.setValue(l["qrWidth"])
        self._txtHeightSp.setValue(l["txtHeight"])
        self._spaceSp.setValue(l["space"])
        self._padSp.setValue(l["pad"])
        self._trimLineCB.setChecked(l["trimLine"] != 0)
        self._multiPrintCB.setChecked(l["multiPrint"] != 0)
        self._dxStepSp.setValue(l["dxStep"])
        self._dyStepSp.setValue(l["dyStep"])
        self._qrx.setValue(l["qrx"])
        self._qry.setValue(l["qry"])
        self._totalWidth.setValue(l["totalWidth"])
        self._totalHeight.setValue(l["totalHeight"])
        self._incrementPolicy.setCurrentIndex(l["incrementPolicy"])

        self._populateFieldPropGrid()

        self._updateIndexToIncrement(l["indexToIncrement"])

        self._skipMarkDirty = prevSkipMarkDirty

    def _updateIndexToIncrement(self, v):
        if self._indexToIncrement.count() == 0:
            return
        indexToIncrementList = []
        for i in range(self._indexToIncrement.count()):
            s = self._indexToIncrement.itemText(i)
            s = s.split(")")[0]
            s = s.strip()
            indexToIncrementList.append(s)

        for x in [v, "21", "10", "250"]:
            if not x in indexToIncrementList:
                continue
            j = indexToIncrementList.index(x)
            self._indexToIncrement.setCurrentIndex(j)
            break
        else:
            self._indexToIncrement.setCurrentIndex(0)

    def _updateUDIInformation(self):
        sources = set()
        for k, v in self._getCurrentLayout()["fieldProp"].items():
            sources.add(v["src"])

        prevIndexToIncrement = self._indexToIncrement.currentText()
        prevIndexToIncrement = prevIndexToIncrement.split(")")[0].strip()
        self._indexToIncrement.clear()

        for aiid, descr, fmt, sdescr in AIList:
            if aiid in sources:
                if sdescr != "":
                    sdescr = " [%s]"%(sdescr)
                self._indexToIncrement.addItem("%6s) %s%s"%(aiid, descr, sdescr))
        self._updateIndexToIncrement(prevIndexToIncrement)

    def _saveDataToLayoutName(self, layoutName):
        d = dict()
        d["qrWidth"] = self._qrWidthSp.value()
        d["txtHeight"] = self._txtHeightSp.value()
        d["space"] = self._spaceSp.value()
        d["pad"] = self._padSp.value()
        d["trimLine"] =  1 if self._trimLineCB.isChecked() else 0
        d["multiPrint"] =  1 if self._multiPrintCB.isChecked() else 0
        d["dxStep"] =  self._dxStepSp.value()
        d["dyStep"] =  self._dyStepSp.value()
        d["qrx"] =  self._qrx.value()
        d["qry"] =  self._qry.value()
        d["totalHeight"] =  self._totalHeight.value()
        d["totalWidth"] =  self._totalWidth.value()
        d["incrementPolicy"] =  self._incrementPolicy.currentIndex()
        s = self._indexToIncrement.currentText()
        s = s.split(")")[0]
        s = s.strip()
        d["indexToIncrement"] =  s

        d["values"] = dict()
        d["fieldProp"] = dict()

        for row, values in self._mapFPRow.items():
            # values = [k, aiid, caption, w2, le, x, y, mode | x2, y2]
            key = values[0]
            aiid = values[1]
            caption = values[2].text()
            value = values[4].text()
            x = values[5].value()
            y = values[6].value()

            d["values"][aiid] = value
            d["fieldProp"][key] = {
                "caption": caption,
                "x": x,
                "y": y,
                "src": aiid
            }
            if aiid.startswith("lb"):
                pass
            elif aiid.startswith("ln"):
                d["fieldProp"][key]["x2"] = values[7].value()
                d["fieldProp"][key]["y2"] = values[8].value()
            else:
                d["fieldProp"][key]["mode"] = values[7].currentText()

        self._layouts[layoutName] = d

    def _saveAs(self):
        layoutName = self._saveAsLe.text()
        if len(layoutName) == 0:
            return

        self._saveDataToLayoutName(layoutName)
        if layoutName != self._layoutId.currentText():
            self._skipMarkDirty = True

            updateComboBox(self._layoutId, list(self._layouts.keys()), layoutName)

            self._loadDataFromCurrentLayout()
            self._skipMarkDirty = False

        #self.layoutUpdated.emit(self._layouts)
        open("layouts.json", "w").write(json.dumps(
                self._layouts, sort_keys=True, indent='    '))

        self._markUpdated()

def main():
    from PySide2.QtWidgets import QApplication
    app = QApplication()
    w = LayoutDialog()
    w.resize(400, 600)
    w.show()
    app.exec_()

if __name__ == "__main__":
    main()
