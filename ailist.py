"""
udi-printer - a tool to print udi datamatrix for label
Copyright (C) 2023 Goffredo Baroncelli <kreijack@inwind.it>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""

from PySide2.QtGui import QValidator
import datetime

AIList = [
    #"AI", "Description",                               "Format", "Short Name"),
    ("00", "Serial Shipping Container Code",            "n2+*n18", "SSCC"),
    ("01", "Global Trade Item Number",                  "n2+*n14", "GTIN"),
    ("02", "GTIN of trade items contained in a logistic unit", "n2+*n14", "CONTENT"),
    ("10", "Batch or lot number ",                      "n2+an..20", "BATCH/LOT"),
    ("11", "Production date (YYMMDD)",                  "n2+d6", "PROD DATE"),
    ("12", "Due date for amount on payment slip (YYMMDD)", "n2+d6", "DUE DATE"),
    ("13", "Packaging date (YYMMDD)",                   "n2+d6", "PACK DATE"),
    ("15", "Best before date (YYMMDD)",                 "n2+d6", "BEST BEFORE or BEST BY"),
    ("17", "Expiration date (YYMMDD)",                  "n2+d6", "USE BY or EXPIRY"),
    ("20", "Internal product variant",                  "n2+n2", "VARIANT"),
    ("21", "Serial number",                             "n2+an..20", "SERIAL"),
    ("22", "Consumer product variant",                  "n2+an..20", "CPV"),
    ("240", "Additional product identification assigned by the manufacturer", "n3+an..30", "ADDITIONAL ID"),
    ("241", "Customer part number",                     "n3+an..30", "CUST. PART NO."),
    ("242", "Made-to-Order variation number",           "n2+n…6", "MTO VARIANT"),
    ("250", "Secondary serial number",                  "n3+an..30", "SECONDARYSERIAL"),
    ("251", "Reference to source entity",               "n3+an..30", "REF. TO SOURCE"),
    ("253", "Global Document Type Identifier",          "n3+*n13+n..17", "GDTI"),
    ("254", "GLN extension component",                  "n3+an..20", "GLN EXTENSION COMPONENT"),
    ("255", "Global Coupon Number",                     "n3+*n13+n..12", "GCN"),
    ("30", "Variable count of items",                   "n2+n..8", "VAR. COUNT"),
#    ("31nn, 32nn, 35nn, 36nn", "Trade measures",        "n4+n6", ""),
#    ("33nn, 34nn, 35nn, 36nn", "Logistic measures",     "n4+n6", ""),
#    ("337n", "Kilograms per square metre",              "n4+n6", "KG PER m²"),
    ("37", "Count of trade items contained in a logistic unit", "n2+n..8", "COUNT"),
#    ("390(n)", "Amount payable or coupon value – Single monetary area", "n4+n..15", "AMOUNT"),
#    ("391(n)", "Amount payable and ISO currency code",  "n4+n3+n..15", "AMOUNT"),
#    ("392(n)", "Amount payable for a variable measure trade item – Single monetary area", "n4+n..15", "PRICE"),
#    ("393(n)", "Amount payable for a variable measure trade item and ISO currency code", "n4+n3+n..15", "PRICE"),
#    ("394(n)", "Percentage discount of a coupon",       "n4+n4", "PRCNT OFF"),
    ("400", "Customer’s purchase order number",         "n3+an..30", "ORDER NUMBER"),
    ("401", "Global Identifier Number for Consignment", "n3+an..30", "GINC"),
    ("402", "Global Shipment Identification Number",    "n3+*n17", "GSIN"),
    ("403", "Routing code",                             "n3+an..30", "ROUTE"),
    ("410", "Ship to – Deliver to Global Location Number", "n3+*n13", "SHIP TO LOC"),
    ("411", "Bill to – Invoice to Global Location Number", "n3+*n13", "BILL TO"),
    ("412", "Purchased from Global Location Number",    "n3+*n13", "PURCHASE FROM"),
    ("413", "Ship for – Deliver for – Forward to Global Location Number", "n3+*n13", "SHIP FOR LOC"),
    ("414", "Identification of a physical location - Global Location Number",
                                                        "n3+*n13", "LOC NO."),
    ("415", "Global Location Number of the invoicing party",
                                                        "n3+n13", "PAY TO"),    ("420", "Ship to – Deliver to postal code within a single postal authority",
                                                        "n3+an..20", "SHIP TO POST"),
    ("421", "Ship to – Deliver to postal code with three-digit ISO country code",
                                                        "n3+n3+an..9", "SHIP TO POST"),
    ("422", "Country of origin of a trade item",        "n3+n3", "ORIGIN"),
    ("423", "Country of initial processing",            "n3+n3+n..12", "COUNTRY – INITIAL PROCESS"),
    ("424", "Country of processing",                    "n3+n3", "COUNTRY – PROCESS"),
    ("425", "Country of disassembly",                   "n3+n3+n..12", "COUNTRY – DISASSEMBLY"),
    ("426", "Country covering full process chain",      "n3+n3", "COUNTRY – FULL PROCESS"),
    ("7001", "NATO Stock Number",                       "n4+n13", "NSN"),
    ("7002", "UN/ECE meat carcasses and cuts classification", "n4+an..30", "MEAT CUT"),
#    ("703(s)", "Approval number of processor with ISO country code", "n4+n3+an..27", "PROCESSOR # s4"),
    ("7003", "Expiration date and time (YYMMDDHHMM)",   "n4+n10", "EXPIRY TIME"),
#    ("710-714", "National Healthcare Reimbursement Number (NHRN)", "n3+n..20", ""),
    ("8001", "Roll products - width, length, core diameter, direction, splices", "n4+n14", "DIMENSIONS"),
    ("8002", "Cellular mobile telephone identifier",    "n4+an..20", "CMT NO."),
    ("8003", "Global Returnable Asset Identifier",      "n4+n14+an..16", "GRAI"),
    ("8004", "Global Individual Asset Identifier",      "n4+an..30", "GIAI"),
    ("8005", "Price per unit of measure",               "n4+n6", "PRICE PER UNIT"),
    ("8006", "Identification of an individual trade item piece", "n4+*n14+n2+n2", "ITIP"),
    ("8007", "International Bank Account Number",       "n4+an..34", "IBAN"),
    ("8008", "Date and time of production (YYMMDDHHMMSS)", "n4+n8+n..4", "PROD TIME"),
    ("8010", "Component/Part Identifier (CPID)",        "n4+an..30", "CPID"),
    ("8011", "Component/Part Identifier Serial Number", "n4+n..12", "CPID SERIAL"),
    ("8012", "Software Version",                        "n4+an..20", "VERSION"),
    ("8013", "Global Model Number (GMN)",               "n4+an..30", "GMN"),
    ("8017", "Global Service Relation Number",          "n4+*n18", "GSRN - PROVIDER"),
    ("8018", "Global Service Relation Number",          "n4+*n18", "GSRN - RECIPIENT"),
    ("8020", "Payment slip reference number",           "n4+an..25", "REF NO."),
    ("8110", "Coupon code identification for use in North America",
                                                        "n4+an..70", ""),
#   ("90", "Information mutually agreed between trading partners",
#                                                        "n2+an..30", "INTERNAL"),
#   ("91-99", "Company internal information",           "n2+an..90", "INTERNAL"),

]

for i in range(1, 10):
    AIList.append( ("lb%02d"%(i), "Custom label %02d"%(i), "l", ""))

AIList.append(("ln", "Line", "l", ""))

AIListDict = dict()
for aiid, descr, fmt, sdescr in AIList:
    AIListDict[aiid] = (aiid, descr, fmt, sdescr)

def validateUDIParam(s, fmt_):
        if fmt_=='l':
            return 0
        i = 0
        digitonly = True
        isDate = False
        for fmt in fmt_[3:].split("+"):
            if fmt.startswith("*"):
                fmt = fmt[1:]
                doDigitCheck = True
            else:
                doDigitCheck = False

            if fmt.startswith("d"):
                isDate = True
                xlen = fmt[1:]
                ccheck = lambda x : x.isdigit()
            elif fmt.startswith("n"):
                xlen = fmt[1:]
                ccheck = lambda x : x.isdigit()
            elif fmt.startswith("an"):
                xlen = fmt[2:]
                # check Figure 7.11-1. GS1 AI encodable character set 82 of GS1
                # General Specifications Standard (2023)
                ccheck = lambda x : (x.isalnum() or x in "!\"%&'()*+,-./:;<=>?_")
            else:
                assert("unknown format" == "'"+fmt+"'")

            if xlen.startswith(".."):
                l = int(xlen[2:])
                lessthan = True
            else:
                l = int(xlen)
                lessthan = False

            assert(not (lessthan and doDigitCheck))

            for j in range(l):
                if i + j>= len(s):
                    break
                if not ccheck(s[i+j]):
                    return 1 # QValidator.Invalid

            if i + l <= len(s) and doDigitCheck:
                if not checkGTIN(s[i:i+l]):
                    return 1 # QValidator.Invalid


            i += l

        if i < len(s):
            # s too long
            return 1 # QValidator.Invalid
        elif i == len(s):
            # exact length
            if isDate:
                ok = True
                try:
                    r = datetime.datetime(year=int("20"+s[0:2]), month = int(s[2:4]), day = int(s[4:6]))
                except:
                    return 1 # QValidator.Invalid
                    
            return 0 # QValidator.Acceptable
        else:
            # s shorter
            if lessthan:
                return 0 # QValidator.Acceptable
            else:
                return 2 # QValidator.Intermediate

# from https://web.archive.org/web/20170527074803/http://www.gs1.org/how-calculate-check-digit-manually
def computeGTIN(s):
    tot = 0

    if len(s) % 2 == 0:
        off = 1
    else:
        off = 0
    for i in range(len(s) - 1):
        if (off + i) % 2 == 0:
            tot += int(s[i]) * 1
        else:
            tot += int(s[i]) * 3

    return 10 - (tot % 10)

def checkGTIN(s):
    return computeGTIN(s) == int(s[-1])

class UDIValidator(QValidator):
    def __init__(self, fmt, parent = None):
        QValidator.__init__(self, parent)
        self._fmt = fmt

    def validate(self, s, pos):
        r = validateUDIParam(s, self._fmt)
        if r == 0:
            return QValidator.Acceptable
        elif r == 1:
            return QValidator.Invalid
        elif r == 2:
            return QValidator.Intermediate
        else:
            assert(False)

def test_checkGTIN():
    # see https://www.gs1.org/services/how-calculate-check-digit-manually
    assert(checkGTIN("6291041500213"))
    assert(not checkGTIN("6291041500212"))
    assert(checkGTIN("12345678901231"))
    assert(not checkGTIN("12345678901232"))


def test_validateUDIParam_nX():
    assert(validateUDIParam("1234", "2n+n4") == 0)  # Acceptable
    assert(validateUDIParam("123", "2n+n4") == 2)   # Intermediate
    assert(validateUDIParam("12367", "2n+n4") == 1)   # Invalid
    assert(validateUDIParam("1x67", "2n+n4") == 1)   # Invalid

def test_validateUDIParam_n_dot_dot_X():
    assert(validateUDIParam("1234", "2n+n..4") == 0)  # Acceptable
    assert(validateUDIParam("123", "2n+n..4") == 0)   # Acceptable
    assert(validateUDIParam("12367", "2n+n..4") == 1)   # Invalid
    assert(validateUDIParam("1x67", "2n+n..4") == 1)   # Invalid

def test_validateUDIParam_d6():
    assert(validateUDIParam("121212", "2n+d6") == 0)  # Acceptable
    assert(validateUDIParam("121", "2n+d6") == 2)   # Intermediate

    # I am not sure if this is right...
    assert(validateUDIParam("123", "2n+d6") == 2)   # Intermediate

    assert(validateUDIParam("12121212", "2n+d6") == 1)   # Invalid
    assert(validateUDIParam("121301", "2n+d6") == 1)   # Invalid
    assert(validateUDIParam("121241", "2n+d6") == 1)   # Invalid

def test_validateUDIParam_anX():
    assert(validateUDIParam("1aa4", "2n+an4") == 0)  # Acceptable
    assert(validateUDIParam("1AA4", "2n+an4") == 0)  # Acceptable
    assert(validateUDIParam("1*&4", "2n+an4") == 0)  # Acceptable
    assert(validateUDIParam("1a3", "2n+an4") == 2)   # Intermediate
    assert(validateUDIParam("1AA78", "2n+an4") == 1)   # Invalid
    assert(validateUDIParam("1 67", "2n+an4") == 1)   # Invalid

def test_validateUDIParam_an_dot_dot_X():
    assert(validateUDIParam("1aa4", "2n+an..4") == 0)  # Acceptable
    assert(validateUDIParam("1A3", "2n+an..4") == 0)   # Acceptable
    assert(validateUDIParam("123L7", "2n+an..4") == 1)   # Invalid
    assert(validateUDIParam("1 67", "2n+an..4") == 1)   # Invalid

def test_validateUDIParam_nXnXnX():
    assert(validateUDIParam("123456", "2n+n4+n2") == 0)  # Acceptable
    assert(validateUDIParam("123", "2n+an4+n2") == 2)   # Intermediate
    assert(validateUDIParam("1234567", "2n+an4+n2") == 1)   # Invalid
    assert(validateUDIParam("1 67", "2n+n4+n2") == 1)   # Invalid

def test_validateUDIParam_nXnXn_dot_dot_X():
    assert(validateUDIParam("123456", "2n+n4+n..2") == 0)  # Acceptable
    assert(validateUDIParam("12345", "2n+n4+n..2") == 0)  # Acceptable
    assert(validateUDIParam("1234567", "2n+an4+n..2") == 1)   # Invalid
    assert(validateUDIParam("1 67", "2n+n4+n..2") == 1)   # Invalid


def test_validateUDIParam_GTIN():
    # see https://www.gs1.org/services/how-calculate-check-digit-manually
    assert(validateUDIParam("6291041500213", "2n+*n13") == 0) # Acceptable
    assert(validateUDIParam("629104150021", "2n+*n13") == 2) # Intermediate
    assert(validateUDIParam("62910415002167", "2n+*n13") == 1) # Invalid
    assert(validateUDIParam("62910415002134", "2n+*n13") == 1) # Invalid
    assert(validateUDIParam("6291041500212", "2n+*n13") == 1)  # Invalid
    assert(validateUDIParam("12346291041500213", "2n+n4+*n13") == 0) # Acceptable
    assert(validateUDIParam("12346291041500213678", "2n+n4+*n13+n3") == 0) # Acceptable
    assert(validateUDIParam("6291041500213678", "2n+*n13+n3") == 0) # Acceptable

if __name__ == "__main__":
    print("test_checkGTIN() ... ", end="")
    test_checkGTIN()
    print("OK")

    print("test_validateUDIParam_nX() ... ", end="")
    test_validateUDIParam_nX()
    print("OK")
    print("test_validateUDIParam_n_dot_dot_X() ... ", end="")
    test_validateUDIParam_n_dot_dot_X()
    print("OK")
    print("test_validateUDIParam_d6() ... ", end="")
    test_validateUDIParam_d6()
    print("OK")
    print("test_validateUDIParam_anX() ... ", end="")
    test_validateUDIParam_anX()
    print("OK")
    print("test_validateUDIParam_an_dot_dot_X() ... ", end="")
    test_validateUDIParam_an_dot_dot_X()
    print("OK")
    print("test_validateUDIParam_nXnXnX() ... ", end="")
    test_validateUDIParam_nXnXnX()
    print("OK")
    print("test_validateUDIParam_nXnXn_dot_dot_X() ... ", end="")
    test_validateUDIParam_nXnXn_dot_dot_X()
    print("OK")
    print("test_validateUDIParam_GTIN() ... ", end="")
    test_validateUDIParam_GTIN()
    print("OK")


