"udi-printer" is a tool to prepare an udi label with a datamatrix SG1 compliant.

It uses the ppf.datamatrix library for the datamatrix rendering. I did a small
change to allow the datamatrix to be GS1 compliant.

The ppf.datamatrix library license is MIT license.
The other code is a GPL V2 license.
